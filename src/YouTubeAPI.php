<?php

namespace Drupal\youtube_integration;

use Google\Client;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides methods to interact with the YouTube API.
 */
class YouTubeAPI {

  /**
   * The YouTube API key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Constructs a new YouTubeAPI object.
   *
   * @param string $apiKey
   *   The YouTube API key.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    // $this->apiKey = $apiKey;
    $config = $configFactory->get('youtube_integration.settings');
    $this->apiKey = $config->get('api_key');
  }

  /**
   * Retrieves video details for a given video ID.
   *
   * @param string $videoId
   *   The YouTube video ID.
   *
   * @return array
   *   An associative array containing video details.
   */
  public function getVideoDetails($videoId) {
    $client = new Client();
    $client->setDeveloperKey($this->apiKey);
    $youtube = new \Google_Service_YouTube($client);

    $videoDetails = $youtube->videos->listVideos('snippet', ['id' => $videoId]);
    // foreach ($videoDetails as $key => $video) {

    //     // dd($video);
    //     print_r("Bigya Ma'am");
    // }
    $video = $videoDetails[0];

    return [
      'title' => $video->snippet->title,
      'description' => $video->snippet->description,
    ];
  }



  public function getAllVideoDetails($playlistId) {
    $client = new Client();
    $client->setDeveloperKey($this->apiKey);
    $youtube = new \Google_Service_YouTube($client);
  
    $videoDetails = [];
  
    $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', [
      'playlistId' => $playlistId,
      'maxResults' => 50, // Adjust this value based on your requirements.
    ]);
  
    foreach ($playlistItemsResponse as $playlistItem) {
      $videoId = $playlistItem['snippet']['resourceId']['videoId'];
      $videoDetailsResponse = $youtube->videos->listVideos('snippet,statistics,contentDetails', ['id' => $videoId]);
  
      foreach ($videoDetailsResponse as $videoDetail) {
        $video = $videoDetail;
  
        $thumbnailUrl = $video->snippet->thumbnails->default->url;
        $views = $video->statistics->viewCount;
        $duration = $this->formatDuration($video->contentDetails->duration);
  
        $videoDetails[] = [
          'id' => $video->id,
          'title' => $video->snippet->title,
          'description' => $video->snippet->description,
          'thumbnail' => $thumbnailUrl,
          'views' => $views,
          'duration' => $duration,
        ];
      }
    }


  
  
    return $videoDetails;
  }
  
  private function formatDuration($duration) {
    preg_match('/PT(\d+H)?(\d+M)?(\d+S)?/', $duration, $matches);
  
    $hours = isset($matches[1]) ? str_replace('H', '', $matches[1]) . ':' : '';
    $minutes = isset($matches[2]) ? str_replace('M', '', $matches[2]) . ':' : '00:';
    $seconds = isset($matches[3]) ? str_replace('S', '', $matches[3]) : '00';
  
    return $hours . $minutes . $seconds;
  }




  
  /**
   * Retrieves all playlists for a given channel ID.
   *
   * @param string $channelId
   *   The YouTube channel ID.
   *
   * @return array
   *   An array of associative arrays containing playlist details.
   */
  

// public function getPlaylists($channelId) {
//   $client = new Client();
//   $client->setDeveloperKey($this->apiKey);
//   $youtube = new \Google_Service_YouTube($client);

//   // $playlists = $youtube->playlists->listPlaylists('snippet', ['channelId' => $channelId]);
//   $playlists = $youtube->playlists->listPlaylists('snippet', [
//     'channelId' => $channelId,
//     'pageToken' => $pageToken,
//     'maxResults' => 50, // You can adjust this based on your needs
// ]);
//   $result = [];

//   $playlistCount = 0; // Counter for the number of playlists fetched

//   foreach ($playlists as $playlist) {
//       if ($playlistCount >= 500) {
//           break; // Break the loop after fetching 10 playlists
//       }

//       // Retrieve the playlist details using the playlist ID
//       $playlistDetails = $youtube->playlists->listPlaylists('snippet', ['id' => $playlist['id']]);
//       $playlistDescription = $playlistDetails[0]['snippet']['description'];
//       // $playlistThumbnail = $playlistDetails[0]['snippet']['thumbnails']['default']['url'];

//       // Choose the highest quality thumbnail available
//       $playlistThumbnails = $playlistDetails[0]['snippet']['thumbnails'];
//       $playlistThumbnail = $playlistThumbnails['maxres']['url'] ?? $playlistThumbnails['standard']['url'] ?? $playlistThumbnails['high']['url'] ?? $playlistThumbnails['default']['url'];

//       $playlistVideos = $youtube->playlistItems->listPlaylistItems('snippet', ['playlistId' => $playlist['id']]);
//       dd($playlistVideos);
//       $videos = [];

//       foreach ($playlistVideos as $playlistVideo) {
//           $videoId = $playlistVideo['snippet']['resourceId']['videoId'];
//           $videoDetails = $this->getVideoDetails($videoId); // Retrieve video details
//           // $videoThumbnail = $playlistVideo['snippet']['thumbnails']['default']['url'];

//           // Choose the highest quality thumbnail available
//           $videoThumbnails = $playlistVideo['snippet']['thumbnails'];
//           $videoThumbnail = $videoThumbnails['maxres']['url'] ?? $videoThumbnails['standard']['url'] ?? $videoThumbnails['high']['url'] ?? $videoThumbnails['medium']['url'] ?? $videoThumbnails['default']['url'];

//           $videos[] = [
//               'id' => $videoId,
//               'title' => $playlistVideo['snippet']['title'],
//               'url' => 'https://www.youtube.com/watch?v=' . $videoId,
//               'description' => $videoDetails['description'], // Add video description
//               'thumbnail' => $videoThumbnail, // Add video thumbnail
//               // Add any other video details you want to retrieve.
//           ];
//       }

//       $result[] = [
//           'id' => $playlist['id'],
//           'title' => $playlist['snippet']['title'],
//           'description' => $playlistDescription, // Add playlist description
//           'thumbnail' => $playlistThumbnail, // Add playlist thumbnail
//           'videos' => $videos,
//       ];

//       $playlistCount++;
//   }

//   return $result;
// }

  

public function getPlaylists($channelId) {
  $client = new Client();
  $client->setDeveloperKey($this->apiKey);
  $youtube = new \Google_Service_YouTube($client);

  // $playlists = $youtube->playlists->listPlaylists('snippet', ['channelId' => $channelId]);
  $playlists = $youtube->playlists->listPlaylists('snippet', [
    'channelId' => $channelId,
    'pageToken' => $pageToken,
    'maxResults' => 50, // You can adjust this based on your needs
]);
  $result = [];

  $playlistCount = 0; // Counter for the number of playlists fetched

  foreach ($playlists as $playlist) {
    if ($playlistCount >= 500) {
        break; // Break the loop after fetching 500 playlists
    }

    // Retrieve the playlist details using the playlist ID
    $playlistDetails = $youtube->playlists->listPlaylists('snippet', ['id' => $playlist['id']]);
    $playlistDescription = $playlistDetails[0]['snippet']['description'];

    // Choose the highest quality thumbnail available
    $playlistThumbnails = $playlistDetails[0]['snippet']['thumbnails'];
    $playlistThumbnail = $playlistThumbnails['maxres']['url'] ?? $playlistThumbnails['standard']['url'] ?? $playlistThumbnails['high']['url'] ?? $playlistThumbnails['default']['url'];

    $playlistVideos = [];
    $pageToken = null;

    do {
        $videoRequest = $youtube->playlistItems->listPlaylistItems('snippet', [
            'playlistId' => $playlist['id'],
            'pageToken' => $pageToken,
            'maxResults' => 50, // Adjust based on your needs
        ]);

        $playlistVideos = array_merge($playlistVideos, $videoRequest->getItems());
        $pageToken = $videoRequest->getNextPageToken();
    } while ($pageToken);

    $videos = [];

    foreach ($playlistVideos as $playlistVideo) {
        $videoId = $playlistVideo['snippet']['resourceId']['videoId'];
        $videoDetails = $this->getVideoDetails($videoId); // Retrieve video details

        // Choose the highest quality thumbnail available
        $videoThumbnails = $playlistVideo['snippet']['thumbnails'];
        $videoThumbnail = $videoThumbnails['maxres']['url'] ?? $videoThumbnails['standard']['url'] ?? $videoThumbnails['high']['url'] ?? $videoThumbnails['medium']['url'] ?? $videoThumbnails['default']['url'];

        $videos[] = [
            'id' => $videoId,
            'title' => $playlistVideo['snippet']['title'],
            'url' => 'https://www.youtube.com/watch?v=' . $videoId,
            'description' => $videoDetails['description'], // Add video description
            'thumbnail' => $videoThumbnail, // Add video thumbnail
            // Add any other video details you want to retrieve.
        ];
    }

    $result[] = [
        'id' => $playlist['id'],
        'title' => $playlist['snippet']['title'],
        'description' => $playlistDescription, // Add playlist description
        'thumbnail' => $playlistThumbnail, // Add playlist thumbnail
        'videos' => $videos,
    ];

    $playlistCount++;
}

  return $result;
}
}
