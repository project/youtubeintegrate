<?php

namespace Drupal\youtube_integration\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the YouTube playlist entity.
 *
 * @ContentEntityType(
 *   id = "youtube_playlist",
 *   label = @Translation("YouTube Playlist"),
 *   base_table = "youtube_playlist",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */

 class YouTubePlaylistEntity extends ContentEntityBase {

    // Add fields to store the playlist data, such as playlist ID, title, description, thumbnail, etc.
  
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
      $fields = parent::baseFieldDefinitions($entity_type);
  
      $fields['playlist_id'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Playlist ID'))
        ->setDescription(t('The ID of the YouTube playlist.'))
        ->setRequired(TRUE)
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -4,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -4,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
  
      $fields['video_id'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Video ID'))
        ->setDescription(t('The ID of the YouTube video in the playlist.'))
        ->setRequired(TRUE)
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -3,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -3,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

        $fields['playlist_title'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Title'))
        ->setDescription(t('The title of the playlist.'))
        ->setRequired(TRUE)
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -2,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -2,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
  
      $fields['video_title'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Title'))
        ->setDescription(t('The title of the video.'))
        ->setRequired(TRUE)
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -2,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -2,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
  
      $fields['description'] = BaseFieldDefinition::create('text_long')
        ->setLabel(t('Description'))
        ->setDescription(t('The description of the video.'))
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'text_default',
          'weight' => -1,
        ])
        ->setDisplayOptions('form', [
          'type' => 'text_textarea',
          'weight' => -1,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
  
      $fields['thumbnail'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Thumbnail URL'))
        ->setDescription(t('The URL of the video thumbnail.'))
        ->setRequired(TRUE)
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
  
      return $fields;
    }
  
  }