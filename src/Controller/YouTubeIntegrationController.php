<?php

namespace Drupal\youtube_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\youtube_integration\YouTubeAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for YouTube Integration module.
 */
class YouTubeIntegrationController extends ControllerBase {

  /**
   * The YouTube API service.
   *
   * @var \Drupal\youtube_integration\YouTubeAPI
   */
  protected $youtubeAPI;

  /**
   * Constructs a new YouTubeIntegrationController object.
   *
   * @param \Drupal\youtube_integration\YouTubeAPI $youtubeAPI
   *   The YouTube API service.
   */
  public function __construct(YouTubeAPI $youtubeAPI) {
    $this->youtubeAPI = $youtubeAPI;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('youtube_integration.youtube_api')
    );
  }

  /**
   * Displays a page listing all YouTube playlists.
   *
   * @return array
   *   Render array for the playlists page.
   */

   /**
 * Fetches the data for all YouTube playlists.
 *
 * @return array
 *   An array of playlist data.
 */
private function fetchUniquePlaylists() {
  $connection = Database::getConnection();

  $query = $connection->select('youtube_playlist', 'p1')
    ->fields('p1', ['playlist_id', 'playlist_title', 'thumbnail', 'description__value'])
    ->distinct();

  $groupedPlaylists = [];

  $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

  foreach ($results as $row) {
    $playlistId = $row['playlist_id'];
    if (!isset($groupedPlaylists[$playlistId])) {
      $groupedPlaylists[$playlistId] = $row;
    }
  }

  return $groupedPlaylists;
}

public function playlists() {
  $playlistData = $this->fetchUniquePlaylists();
  // dd($playlistData);
  $build = [];

  // Render the list of playlists.
  foreach ($playlistData as $playlistId => $playlistInfo) {
    // dd($playlistInfo);
    $playlistUrl = Url::fromRoute('youtube_integration.playlist_videos', ['playlistId' => $playlistId])->toString();

    $build[] = [
      '#markup' => '<div class="playlist-item">
          <a href="' . $playlistUrl . '">
            <img src="' . $playlistInfo['thumbnail'] . '" alt="' . $playlistInfo['title'] . '" />
            <h3>' . $playlistInfo['playlist_title'] . '</h3>
          </a>
        </div>',
    ];
  }

  return [
    '#theme' => 'item_list',
    '#items' => $build,
    '#attributes' => ['class' => ['playlist-items']],
  ];
}





  /**
   * Displays a page for playing a YouTube video.
   *
   * @param string $playlistId
   *   The playlist ID.
   * @param string $videoId
   *   The video ID.
   *
   * @return array
   *   Render array for the video page.
   */

   private function fetchPlaylistVideo($playlistId, $videoId) {
    $connection = Database::getConnection();

    $query = $connection->select('youtube_playlist', 'p')
      ->fields('p', ['video_id', 'video_title', 'thumbnail'])
      ->condition('p.playlist_id', $playlistId)
      ->condition('p.video_id', $videoId)
      ->distinct()
      ->orderBy('p.video_title', 'ASC');

    return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
  }


  private function fetchPlaylistVideos($playlistId) {
    $connection = Database::getConnection();

      // Get the current request from the request stack.
      $current_request = \Drupal::requestStack()->getCurrentRequest();

      // Get the value of a specific query parameter.
      $page = ($current_request->query->get('page')) ? $current_request->query->get('page') : 0 ;



      // dd($param_value);

    // Set the number of items per page.
    $itemsPerPage = 10; // Display 2 videos per page.

    // Calculate the offset based on the current page.
    $offset = $page * $itemsPerPage;

    $query = $connection->select('youtube_playlist', 'p')
      ->fields('p', ['video_id', 'video_title', 'thumbnail'])
      ->condition('p.playlist_id', $playlistId)
      ->distinct()
      ->orderBy('p.video_title', 'ASC')
      ->range($offset, $itemsPerPage);

      // dd($query->execute()->fetchAll(\PDO::FETCH_ASSOC));

    return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
}


  public function playVideo($playlistId, $videoId) {
    // Load the YouTube playlist video using the playlist ID and video ID.
    $playlistVideo = $this->fetchPlaylistVideo($playlistId, $videoId);

    // dd($playlistVideo);
    if (!empty($playlistVideo)) {
      // $videoDetails = $this->youtubeAPI->getVideoDetails($videoId);
      $videoPlayerMarkup = '<iframe width="100%" height="700px" src="https://www.youtube.com/embed/' . $videoId . '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
      $videoWithDetailsMarkup = '<h3 class="youtube-integration-single-video">' . $playlistVideo[0]['video_title'] . '</h3>' .$videoPlayerMarkup ;

      return [
        '#type' => 'markup',
        '#markup' => Markup::create($videoWithDetailsMarkup),
      ];
    } else {
      return [
        '#type' => 'markup',
        '#markup' => 'Video not found!',
      ];
    }
  }


  // private function fetchPlaylistVideos($playlistId) {
  //   $connection = Database::getConnection();

  //   $query = $connection->select('youtube_playlist', 'p')
  //     ->fields('p', ['video_id', 'video_title', 'thumbnail','description__value'])
  //     ->condition('p.playlist_id', $playlistId)
  //     ->distinct()
  //     ->orderBy('p.video_title', 'ASC');

  //   return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
  // }


public function playlistVideos($playlistId) {
  $playlist = $this->fetchPlaylistVideos($playlistId);
  // dd($playlist);
  $build = [];

  // Render the list of videos.
  foreach ($playlist as $video) {
    $videoId = $video['video_id'];
    $videoUrl = Url::fromRoute('youtube_integration.play_video', [
      'playlistId' => $playlistId,
      'videoId' => $videoId,
    ])->toString();

    $thumbnail_image = $video['thumbnail'];

    // $videoPlayerMarkup = '<iframe width="560" height="200" src="https://www.youtube.com/embed/' . $videoId . '" frameborder="0" allowfullscreen></iframe>';
    $thumbnailMarkup = '<img width="560" height="200" src="' . $thumbnail_image . '" >';
    $videoWithDetailsMarkup = '<a href="' . $videoUrl . '">'. $thumbnailMarkup .' <h3>' . $video['video_title'] . '</h3></a>';

    $build[] = [
      '#type' => 'markup',
      '#markup' => Markup::create($videoWithDetailsMarkup),
    ];
  }

  return [
    '#theme' => 'item_list',
    '#items' => $build,
    '#attributes' => ['class' => ['videos-items']],
  ];
}

  public function playlistVideosTitle($playlistId) {
    // Retrieve playlist title for the page title.
    $playlists = $this->youtubeAPI->getPlaylists('UCl2_PpPp-1O0takJW4dJL6g');
    foreach ($playlists as $playlist) {
      if ($playlist['id'] == $playlistId) {
        return $playlist['title'] . ' Videos';
      }
    }
    return '';
  }

}
